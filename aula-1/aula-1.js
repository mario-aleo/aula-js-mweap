var lala = 0;

nomeada(0, 1);

function nomeada(x, y) {
	console.log(x);
	console.log(y);
}

nomeada(0, 1);

// anomima(0, 1); resulta em erro;

var anonima = function (x, y) {
	console.log(x);
	console.log(y);
};

anonima(0, 1);

(function () {
	console.log('instantanea');
})();

(function (x, y) {
	console.log(x);
	console.log(y);
})('instantanea', lala);

function MinhaClasse(nome, idade) {
	this.nome = nome || '';
	this.idade = idade || 0;

	this.apresentar = function () {
		console.log(
			'Meu nome é ' + this.nome +
			' , tenho ' + this.idade + ' anos.'
		);
	};
}

var obj = new MinhaClasse('Mario', 24);

console.log(obj.nome);
console.log(obj.idade);
obj.apresentar();

obj.nome = 'Antonio';
obj.idade = 27;

console.log(obj.nome);
console.log(obj.idade);
obj.apresentar();

var overload = { nome: 'Maria', idade: 22 };
//obj.apresentar.bind(overload);
obj.apresentar.bind(overload)();