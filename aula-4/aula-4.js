var x = 2;
var y = 7;
new Promise((resolve, reject) => {
  const sum = x + y;
  if (sum <= 10)
    reject(sum);
  else
    resolve(sum);
}).then(res => {
  console.log(`OK: ${res} é maior que 10`);
}).catch(err => {
  console.log(`Erro: ${err} é menor ou igual a 10`);
});


function lala (x, y, callback) {
  callback(x + y);
}
function log (x) {
  console.log(x);
}
lala(2, 5, log);
// 7

console.log('Checkpoint 1');
var x = 5;
var y = 7;
new Promise((resolve, reject) => {
  const sum = x + y;
  console.log('Checkpoint 2');
  if (sum <= 10)
    reject(sum);
  else
    resolve(sum);
}).then(res => {
  console.log(`OK: ${res} é maior que 10`);
  console.log('Checkpoint 3');
}).catch(err => {
  console.log('Checkpoint 4');
  console.log(`Erro: ${err} é menor ou igual a 10`);
});
console.log('Checkpoint 5');

console.log('Checkpoint 1');
var x = 2;
var y = 7;
new Promise((resolve, reject) => {
  const sum = x + y;
  console.log('Checkpoint 2');
  if (sum <= 10)
    reject(sum);
  else
    resolve(sum);
}).then(res => {
  console.log(`OK: ${res} é maior que 10`);
  console.log('Checkpoint 3');
}).catch(err => {
  console.log('Checkpoint 4');
  console.log(`Erro: ${err} é menor ou igual a 10`);
});
console.log('Checkpoint 5');

console.log('Checkpoint 1');
var x = 5;
var y = 7;
new Promise((resolve, reject) => {
  const sum = x + y;
  console.log('Checkpoint 2');
  if (sum <= 10)
    reject(sum);
  else
    resolve(sum);
}).then(res => {
  console.log('Checkpoint 3');
  return `OK: ${res} é maior que 10`;
}).then(res => {
  console.log(res);
  console.log('Checkpoint 4');
}).catch(err => {
  console.log('Checkpoint 5');
  return `Erro: ${err} é menor ou igual a 10`;
}).then(res => {
  console.log(res);
  console.log('Checkpoint 6');
});
console.log('Checkpoint 7');

var x = 5;
var y = 1;
new Promise((resolve, reject) => {
  const sum = x + y;
  console.log('Checkpoint 2');
  if (sum <= 10)
    reject(sum);
  else if (sum <= 20)
    resolve(sum);
}).then(res => {
  console.log('Checkpoint 3');
  return `OK: ${res} é maior que 10`;
}).then(res => {
  console.log(res);
  console.log('Checkpoint 4');
}).catch(err => {
  console.log('Checkpoint 5');
  return `Erro: ${err} é menor ou igual a 10`;
}).then(res => {
  console.log(res);
  console.log('Checkpoint 6');
});
console.log('Checkpoint 7');

