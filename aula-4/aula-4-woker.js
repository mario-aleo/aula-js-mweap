importScripts('/aula-4/aula-4-soma.js');

onmessage = mensagemEvt => {
  const mensagem = mensagemEvt.data;
  switch (mensagem.action) {
    case 'soma':
      postMessage(
        soma(mensagem.data.x, mensagem.data.y)
      );
      break;
    default:
      close();
      break;
  }
};

// Criar ações de Subtração, Multiplicação, Divisão
// Utilizar importScripts