const worker = new Worker('aula-4/aula-4-woker.js');

worker.onmessage = mensagemEvt => {
  console.log(mensagemEvt.data);

  worker.terminate();
};

worker.postMessage(
  {action: 'soma', data: { x: 1, y: 2 }}
);