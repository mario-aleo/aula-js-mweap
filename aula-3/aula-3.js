class Pessoa {
  constructor(nome, idade) {
    this.nome = nome;
    this.idade = idade;
  }

  dizerNome() {
    console.log(this.nome);
  }

  dizerIdade() {
    console.log(this.idade);
  }

  _funcaoPrivada() {
    console.log('privada');
  }

  __funcaoProtegida() {
    console.log('protegido');
  }
}

class Joao extends Pessoa {
  constructor(idade, sexo = 'masculino') {
    super('João', idade);
    this.sexo = sexo;
  }

  dizerNome() {
    console.log(`Meu nome é ${(() => this.nome)()}`);
  }

  dizerIdade() {
    console.log(`Tenho ${this.idade} anos`);
  }

  apresentacao() {
    this.dizerNome();
    this.dizerIdade();
  }

  _funcaoPrivada() {
    super._funcaoPrivada();
    console.log('Só para lembrar');
  }
}

class Jarra {
  constructor() {
    this._aberta = false;
    this._volume = 0;
  }

  get volume() {
    return this._volume;
  }
  set volume(valor) {
    if (typeof valor == 'number')
      this._volume = valor;
  }

  get aberta() {
    return this._aberta;
  }
  set aberta(valor) {
    if (typeof valor == 'boolean')
      this._aberta = valor;
  }

  static descricao() {
    console.log(`${this.volume}L`);
    console.log(`${this.aberta ? 'Aberta' : 'Fechada'}`);
  }
}

let maria = new Pessoa('Maria', 23);
console.log(maria.nome);
console.log(maria.idade);
maria.dizerNome();
maria.dizerIdade();

let joao = new Joao(26);
console.log(maria.nome);
console.log(maria.idade);
maria.apresentacao();
