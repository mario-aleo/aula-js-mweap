let a = 1;
//let a = 0; erro

const b = 0;
//const b = 1; erro
//b = 1;

const c = [1, 2, 3];
//const c = [2, 3, 4]; erro
//c = [2, 3, 4]; erro
//c[0] = 5; certo

const d = {a: 1, b: 2, c:3};
//const d = {a: 1, b: 2, c:3}; erro
//d = {a: 1, b: 2, c:3}; erro
//d.a = 'a'; certo

const f = c[0];
//1
c[0] = 2;
//1
const h = c;
//h[1] 2
c[1] = 3;
//h[1] = 3;  

function lala() {
  let a = 0;
  this.a = 2;
}

function momo() {
  const c = [];
}

//const anonima = (a, b) => {
//  return a + b;
//};
const anonima = (a, b) => a + b;
anonima(1, 2);
// 3
const soma1 = a => a + 1;
soma1(5);
// 6
const logLala = () => console.log('lala');
logLala();
// lala
(() => console.log('momo'))();