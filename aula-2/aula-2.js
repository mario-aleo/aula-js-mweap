var arr = ['a', 'b', 'c'];
console.log(arr[2]); // 'c'

var mat = [
  ['a', 'b', 'c'],
  ['d', 'e', 'f'],
  ['g', 'h', 'i']
];
console.log(mat[1]); // ['d', 'e', 'f']
console.log(mat[1][1]); // 'e'

for (var i = 0; i < arr.length; i++) {
  console.log(arr[i]);
}
// 'a'
// 'b'
// 'c'

var wi = 0;
while(wi < arr.length) {
  console.log(arr[wi]);
  wi = wi + 1;
}
// 'a'
// 'b'
// 'c'

arr.forEach(function(ele, index, arr) {
  console.log(ele);
  console.log(index);
  console.log(arr);
});
// 'a' 0 ['a', 'b', 'c']
// 'b' 1 ['a', 'b', 'c']
// 'c' 2 ['a', 'b', 'c']

mat.forEach(function(ele, index, arr) {
  console.log(ele);
  console.log(index);
  console.log(arr);

  ele.forEach(function(ele1, index1, arr1) {
    console.log(ele1);
    console.log(index1);
    console.log(arr1);
  });
});
// ['a', 'b', 'c'] 0 [['a', 'b', 'c'], ['d', 'e', 'f'], ['g', 'h', 'i']]
// 'a' 0 ['a', 'b', 'c']
// 'b' 1 ['a', 'b', 'c']
// 'c' 2 ['a', 'b', 'c']

// ['d', 'e', 'f'] 1 [['a', 'b', 'c'], ['d', 'e', 'f'], ['g', 'h', 'i']]
// 'd' 0 ['d', 'e', 'f']
// 'e' 1 ['d', 'e', 'f']
// 'f' 2 ['d', 'e', 'f']

// ['g', 'h', 'i'] 2 [['a', 'b', 'c'], ['d', 'e', 'f'], ['g', 'h', 'i']]
// 'g' 0 ['g', 'h', 'i']
// 'h' 1 ['g', 'h', 'i']
// 'i' 2 ['g', 'h', 'i']

var arrEle = arr.map(function(ele, index, arr) {
  console.log(ele);
  console.log(index);
  console.log(arr);
  return ele;
});
console.log(arrEle);
// 'a' 0 ['a', 'b', 'c']
// 'b' 1 ['a', 'b', 'c']
// 'c' 2 ['a', 'b', 'c']
// ['a', 'b', 'c']

var arrAB = arr.map(function(ele, index, arr) {
  if (ele != 'c')
    return ele;
});
console.log(arrAB);
// ['a', 'b']

var arrIndex = arr.map(function(ele, index, arr) {
  return index;
});
console.log(arrIndex);
// [0, 1, 2]

var arrBC = arr.filter(function(ele, index, arr) {
  return ele != 'a';
});
console.log(arrBC);
// ['b', 'c']

var arr12 = arr.map(function(ele, index) {
  return index;
}).filter(function(ele) {
  return ele > 0;
});
console.log(arr12);
//  [1, 2]

var arrNum = [1, 1, 2, 3, 5, 8, 13, 21];

var soma = arrNum.reduce(
  function(acc, ele, index, arr) {
    return acc + ele;
  },
  0 // 0, [], '', {}
);
console.log(soma);

var str = arr.reduce(
  function(acc, ele, index, arr) {
    return acc + ' ' + ele;
  },
  '' // 0, [], '', {}, true
);
console.log(str);
// 'a b c'

// ==   - valor igual
// ===  - valor igual, tipo igual
// !=   - valor diferente 
// !==  - valor diferente, tipo igual
// !    - negando a existencia de valor (0, null, undefined, '' sem valor)
// ||   - se não tiver
// ? :  - if else rapido 
var a = 0;
!a;
// true
!!a;
// false
var b = false;
!b;
// true
b = true;
!b;
// false
!!b;
// true
var c = a || 2;
// 2
var d = b || 'd';
// true
'1' == 1 ? true : false;
// true
'1' === 1 ? true : false;
// false