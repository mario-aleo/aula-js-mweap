window.slideShow = {
  pagina: 0,
  autoPlay: false,
  autoPlayInterval: null
}

passar(0);

window.addEventListener('keydown', evt => {
  switch (evt.key) {
    case 'ArrowLeft':
      slideShow.pagina -= 1;
      passar(slideShow.pagina);
      break;
    case 'ArrowRight':
      slideShow.pagina += 1;
      passar(slideShow.pagina);
      break;
    case ' ':
      playPause();
      break;
    default:
      break;
  }
});

function playPause () {
  if (slideShow.autoPlay) {
    slideShow.autoPlay = !slideShow.autoPlay;
    clearInterval(slideShow.autoPlayInterval);
    return;
  }

  const paginas = document.querySelectorAll('[pagina]');
  const ultimaPagina = (
    parseInt(paginas[paginas.length - 1].getAttribute('pagina'))
  );
  
  slideShow.autoPlay = !slideShow.autoPlay;
  
  slideShow.autoPlayInterval = setInterval(
    () => {
      if (slideShow.pagina + 1 > ultimaPagina) {
        slideShow.autoPlay = !slideShow.autoPlay;
        clearInterval(slideShow.autoPlayInterval);
      }
      passar(slideShow.pagina + 1);
    },
    2000
  );
}

function passar (pagina) {
  const paginas = document.querySelectorAll('[pagina]');
  const primeiraPagina = (
    parseInt(paginas[0].getAttribute('pagina'))
  );
  const ultimaPagina = (
    parseInt(paginas[paginas.length - 1].getAttribute('pagina'))
  );
  
  if (pagina > ultimaPagina)
    pagina = primeiraPagina;
  else if (pagina < primeiraPagina)
    pagina = ultimaPagina;
  
  const ativa = document.querySelector('[ativa]');
  const proximaAtiva = document.querySelector(`[pagina="${pagina}"]`);
  
  if (!proximaAtiva)
    return;
  
  if (ativa)
    ativa.removeAttribute('ativa');

  proximaAtiva.setAttribute('ativa', '');
  
  slideShow.pagina = pagina;
}

